package com.gitee.hermer.boot.jee.api;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.gitee.hermer.boot.jee.api.domain.BaseInfo;
import com.gitee.hermer.boot.jee.commons.reflect.ClassUtils;
import com.gitee.hermer.boot.jee.service.IBaseService;
import com.github.pagehelper.PageInfo;

public  class RestBaseController<T, ID extends Serializable, Service extends IBaseService<T, ID>> extends BaseControllerHandler
{
	@Autowired
	private Service serviceImpl;

	public Service getService() throws Throwable {
		return ClassUtils.getTargetProxy(serviceImpl);
	}

	public PageInfo<T> pageList(T t, Integer pageNum, Integer pageSize)
			throws Throwable {
		return serviceImpl.pageList(t, pageNum, pageSize);
	}
	
	public BaseInfo<T> list(T t)
			throws Throwable {
		return new BaseInfo<T>(getProtocolVersion(), serviceImpl.list(t));
	}

	public BaseInfo<T> save(T t) throws Throwable {
		serviceImpl.insert(t);
		return new BaseInfo<T>(getProtocolVersion(), t);
	}

	public BaseInfo<T> update(T t) throws Throwable {
		serviceImpl.update(t);
		return new BaseInfo<T>(getProtocolVersion(), t);

	}

	public BaseInfo<T> delete(T t) throws Throwable {
		serviceImpl.delete(t);
		return new BaseInfo<T>(getProtocolVersion(), t);
	}

	public BaseInfo<T> find(T t) throws Throwable {
		return new BaseInfo<T>(getProtocolVersion(), serviceImpl.find(t));
	}

	public BaseInfo<T> deleteBatch(List<T> list) throws Throwable {
		serviceImpl.deleteBatch(list);
		return new BaseInfo<T>(getProtocolVersion(), list);
	}

	public BaseInfo<T> insertBatch(List<T> list) throws Throwable {
		serviceImpl.insertBatch(list);
		return new BaseInfo<T>(getProtocolVersion(), list);
	}

	public BaseInfo<T> updateBatch(List<T> list) throws Throwable {
		serviceImpl.updateBatch(list);
		return new BaseInfo<T>(getProtocolVersion(), list);
	}
	
	public BaseInfo<T> find(ID id) throws Throwable {
		return new BaseInfo<T>(getProtocolVersion(), serviceImpl.find(id));
	}

}
