package com.gitee.hermer.boot.jee.orm.ibatis.locker.cache;

import java.util.concurrent.ConcurrentHashMap;

import com.gitee.hermer.boot.jee.commons.log.UtilsContext;
import com.gitee.hermer.boot.jee.orm.annotation.Version;

/**
 * 
 * @ClassName: LocalVersionLockerCache
 * @Description: 乐观锁版本缓存
 * @author:  涂孟超
 * @date: 2017年10月7日 下午3:06:22
 */
public class LocalVersionLockerCache  extends UtilsContext implements VersionLockerCache {
	
	private ConcurrentHashMap<String, ConcurrentHashMap<VersionLockerCache.MethodSignature, Version>> caches = new ConcurrentHashMap<>();
	
	@Override
	public boolean containMethodSignature(MethodSignature vm) {
		String nameSpace = getNameSpace(vm);
		ConcurrentHashMap<VersionLockerCache.MethodSignature, Version> cache = caches.get(nameSpace);
		if(null == cache || cache.isEmpty()) {
			return false;
		}
		boolean containsMethodSignature = cache.containsKey(vm);
		if(containsMethodSignature && isDebugEnabled()) {
			debug("The method " + nameSpace + vm.getId() + "is hit in cache.");
		}
		return containsMethodSignature;
	}
	
	// 这里去掉synchronized或者重入锁，因为这里的操作满足幂等性
	// Here remove synchronized keyword or ReentrantLock, because it's a idempotent operation
	@Override
	public void cacheMethod(VersionLockerCache.MethodSignature vm, Version locker) {
		String nameSpace = getNameSpace(vm);
		ConcurrentHashMap<VersionLockerCache.MethodSignature, Version> cache = caches.get(nameSpace);
		if(null == cache || cache.isEmpty()) {
			cache = new ConcurrentHashMap<>();
			cache.put(vm, locker);
			caches.put(nameSpace, cache);
			if(isDebugEnabled()) {
				debug("Locker debug info ==> " + nameSpace + ": " + vm.getId() + " is cached.");
			}
		} else {
			cache.put(vm, locker);
		}
	}

	@Override
	public Version getVersionLocker(VersionLockerCache.MethodSignature vm) {
		String nameSpace = getNameSpace(vm);
		ConcurrentHashMap<VersionLockerCache.MethodSignature, Version> cache = caches.get(nameSpace);
		if(null == cache || cache.isEmpty()) {
			return null;
		}
		return cache.get(vm);
	}

	private String getNameSpace(VersionLockerCache.MethodSignature vm) {
		String id = vm.getId();
		int pos = id.lastIndexOf(".");
		return id.substring(0, pos);
	}

}