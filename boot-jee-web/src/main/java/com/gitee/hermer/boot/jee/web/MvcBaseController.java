package com.gitee.hermer.boot.jee.web;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import com.gitee.hermer.boot.jee.commons.reflect.ClassUtils;
import com.gitee.hermer.boot.jee.service.IBaseService;


public abstract  class MvcBaseController<T, ID extends Serializable, Service extends IBaseService<T, ID>> extends BaseControllerHandler{

	@Autowired
	private Service serviceImpl;

	public Service getService() throws Throwable {
		return ClassUtils.getTargetProxy(serviceImpl);
	}

	public abstract ModelAndView list(T t)
			throws Throwable;
	public abstract ModelAndView pageList(T t, Integer pageNum, Integer pageSize)
			throws Throwable;
	public abstract ModelAndView save(T t)
			throws Throwable;
	public abstract ModelAndView update(T t)
			throws Throwable;
	public abstract ModelAndView delete(T t)
			throws Throwable;
	public abstract ModelAndView find(T t)
			throws Throwable;
	public abstract ModelAndView deleteBatch(T t)
			throws Throwable;
	public abstract ModelAndView insertBatch(T t)
			throws Throwable;
	public abstract ModelAndView updateBatch(T t)
			throws Throwable;
	public abstract ModelAndView find(ID id) 
			throws Throwable;

}
