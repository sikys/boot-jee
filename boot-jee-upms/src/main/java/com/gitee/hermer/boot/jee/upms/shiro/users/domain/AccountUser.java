package com.gitee.hermer.boot.jee.upms.shiro.users.domain;

import java.io.Serializable;
import com.gitee.hermer.boot.jee.orm.annotation.Id;

public class AccountUser implements Serializable {

	private String jobUuid;
	private Integer enable;
	private String userName;
	@Id
	private Integer id;
	private String password;
	private String nickName;

	public void setJobUuid(String jobUuid){
		this.jobUuid=jobUuid;
	}
	public String getJobUuid(){
		return jobUuid;
	}
	public void setEnable(Integer enable){
		this.enable=enable;
	}
	public Integer getEnable(){
		return enable;
	}
	public void setUserName(String userName){
		this.userName=userName;
	}
	public String getUserName(){
		return userName;
	}
	public void setId(Integer id){
		this.id=id;
	}
	public Integer getId(){
		return id;
	}
	public void setPassword(String password){
		this.password=password;
	}
	public String getPassword(){
		return password;
	}
	public void setNickName(String nickName){
		this.nickName=nickName;
	}
	public String getNickName(){
		return nickName;
	}
}
