package com.gitee.hermer.boot.jee.ngrok;

public class NgrokMain {

    private static final String serverAddress = "tunnel.qydev.com";
    private static final int serverPort = 4443;

    public static void main(String[] args) throws Exception {
        Tunnel tunnel = new Tunnel.TunnelBuild()
                .setPort(8080).setProto("http").setSubDomain("test11").build();
        new NgrokClient(serverAddress, serverPort)
                .addTunnel(tunnel).start();

    }

}
