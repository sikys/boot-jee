package com.gitee.hermer.boot.jee.cache.redis.support;

import redis.clients.jedis.JedisPool;

import java.io.IOException;

import com.gitee.hermer.boot.jee.cache.redis.client.SingleRedisClient;


public class RedisSingleFactory implements RedisClientFactory<SingleRedisClient> {

    private static JedisPool jedisPool;
    private RedisPoolConfig poolConfig;

    public synchronized JedisPool getJedisPool() {
        return jedisPool;
    }

    @Override
    public SingleRedisClient getResource() {
        return new SingleRedisClient(getJedisPool().getResource());
    }

    @Override
    public void returnResource(SingleRedisClient client) {
        if (client != null)
            client.close();
    }

    @Override
	public void build() {
        String host = this.poolConfig.getHost();
        int port = this.poolConfig.getPort();
        int timeout = this.poolConfig.getTimeout();
        int database = this.poolConfig.getDatabase();
        String password = this.poolConfig.getPassword();
        if (password != null && !"".equals(password))
            jedisPool = new JedisPool(poolConfig, host, port, timeout, password, database);
        else {
        	jedisPool = new JedisPool(poolConfig, host, port, timeout, null, database);
        }
    }

    public RedisPoolConfig getPoolConfig() {
        return this.poolConfig;
    }

    public void setPoolConfig(RedisPoolConfig poolConfig) {
        this.poolConfig = poolConfig;
    }

    
    @Override
    public void close() throws IOException {
        jedisPool.close();
    }
}
