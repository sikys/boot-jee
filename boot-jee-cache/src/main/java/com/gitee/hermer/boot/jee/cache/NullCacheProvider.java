package com.gitee.hermer.boot.jee.cache;

import java.util.Properties;


public class NullCacheProvider implements CacheProvider {

	private final static NullCache cache = new NullCache();

	@Override
	public String name() {
		return "none";
	}

	
	@Override
	public Cache buildCache(String regionName, boolean autoCreate,
			CacheExpiredListener listener) throws CacheException {
		return cache;
	}

	
	@Override
	public void start(Properties props) throws CacheException {
	}

	
	@Override
	public void stop() {
	}

}
